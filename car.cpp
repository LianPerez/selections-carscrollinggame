#include "car.h"


///
/// Default Constructor for the racer class. Properties are set as follow:
/// * y_car: The starting Y coordinate of the racecar
///
Car::Car(QWidget *parent) :
    QWidget(parent)
{
    y_car=80;
    name = ":/resources/racer.png" ;
}

///
/// Setter for the racecar
///
void Car::setCar(string arg){
    
    if (arg == "red") name = ":/resources/racer.png";
    
    else if(arg== "pink") name = ":/resources/racer2.png";
    
    else if(arg== "green") name = ":/resources/racer3.png";
    
    else if(arg == "orange") name = ":/resources/racer4.png";
    
    else if(arg == "candy") name = ":/resources/sweetcar.png";
    
    else if(arg == "batman") name = ":/resources/batman.png";
    
    else name = ":/resources/racer5.png";
    

    repaint();
}

///
/// Getter for the racecar
///
string Car::getCar(){
    return name;
}

///
/// Setter for the Y coordinate of the racecar
///
void Car::setYCar(int arg){
    y_car = arg;
}

///
/// Getter for the Y coordinate of the racecar
///
int Car::getYCar(){
    return y_car;
}
